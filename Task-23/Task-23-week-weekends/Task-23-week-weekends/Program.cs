﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_23_week_weekends
{
    class Program
    {
        static void Main(string[] args)
        {

            var weeks = new Dictionary<string, string>();

            weeks.Add("Monday", "Weekday");
            weeks.Add("Tuesday", "Weekday");
            weeks.Add("Wednesday", "Weekday");
            weeks.Add("Thursday", "Weekday");
            weeks.Add("Friday", "Weekday");
            weeks.Add("Saturday", "Weekend");
            weeks.Add("Sunday", "Weekend");

            var wday = new List<string> { };
            var wend = new List<string> { };

            foreach (var x in weeks)
            {
                if (x.Value == "Weekday")
                {
                    wday.Add(x.Key);
                }
                else
                {
                    wend.Add(x.Key);

                }
            }
            for (var i = 0; i < wday.Count;)
            {
                Console.WriteLine($"{wday[i]} Is a Weekday");
                i++;
            }
            for (var i = 0; i < wend.Count;)
            {
                Console.WriteLine($"{wend[i]} Is a Weekend");
                i++;
            }


        }
    }
}
