﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_29_count_array
{
    class Program
    {
        static void Main(string[] args)
        {
            var words = new string[8] { "The", "quick", "brown", "fox", "jumped", "over", "the", "fence" };
            var count = words.Length;
            Console.WriteLine($"There are {count} Words in the string");
        }
    }
}
