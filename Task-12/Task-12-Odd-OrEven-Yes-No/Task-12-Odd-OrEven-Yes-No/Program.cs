﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12_Odd_Or_Even_Yes_No
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Code adds 2 to both even and odd variables until it is equal to Unumber(The user inputed number), it will display Yes if Even and No if Odd */

            Console.WriteLine("Input a number to check if it is even or odd. ");
            var Unumber = int.Parse(Console.ReadLine());
            int even = 0;
            int odd = 1;
            Console.Clear();

            for (even = 0; Unumber > even;) /* Starts at 0 and adds 2 to keep it an even number until it is equal to the User inputed number */
            {
                even = even + 2;
            }

            for (odd = 1; Unumber > odd;)  /* Starts at 1 and adds 2 to keep it an odd number until it is equal to the User inputed number */
            {
                odd = odd + 2;
            }

            if (Unumber == even)
            {
                Console.WriteLine("Yes.");
            }
            if (Unumber == odd)
            {
                Console.WriteLine("No.");
            }

        }
    }
}
