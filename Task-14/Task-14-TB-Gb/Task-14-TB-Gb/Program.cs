﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_14_TB_Gb
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please type in amount of TB to convert to GB.");
            int TB = int.Parse(Console.ReadLine());

            int GB = 1024;

            var convert = TB * GB;
            Console.Clear();
            if (TB > 0)
            {

                Console.WriteLine($"{TB} Terabytes is equal to {convert} Gigabytes."); /* Code converts Terabytes into Gigabytes */

            }

            else
            {
                Console.WriteLine("Invalid Number");
            }
        }
    }
}
