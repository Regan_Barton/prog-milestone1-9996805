﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_22_fruit_dictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            var dict = new Dictionary<string, string>();

            dict.Add("Orange", "Fruit");
            dict.Add("Apple", "Fruit");
            dict.Add("Pear", "Fruit");
            dict.Add("Carrot", "Vegetable");
            dict.Add("Cabbage", "Vegetable");
            dict.Add("Bananna", "Fruit");
            dict.Add("Lemon", "Fruit");
            dict.Add("Broccoli", "Vegetable");
            dict.Add("Onion", "Vegetable");
            dict.Add("Spinach", "Vegetable");

            var fruit = new List<string> { };
            var vege = new List<string> { };

            foreach (var x in dict)
            {
                if (x.Value == "Fruit")
                {
                    fruit.Add(x.Key);
                }
                else
                {
                    vege.Add(x.Key);

                }
            }

            Console.WriteLine($"");
            Console.WriteLine($"There are {fruit.Count} fruits.");
            Console.WriteLine($"There are {vege.Count} vegetables.");
            Console.WriteLine();
            for (var i = 0; i < fruit.Count;)
            {
                Console.WriteLine($"fruits are {fruit[i]}");
                i++;
            }

        }

    }
}

