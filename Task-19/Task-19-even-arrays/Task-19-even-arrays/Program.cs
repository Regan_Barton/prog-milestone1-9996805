﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_19_even_arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = new int[7] { 34, 45, 21, 44, 67, 88, 86 };

            var even = new List<int> { };
            even.Add(numbers[0]);
            even.Add(numbers[3]);
            even.Add(numbers[5]);
            even.Add(numbers[6]);

            foreach(var e in even)
            {
                Console.WriteLine(e);
            }
        }
    }
}
