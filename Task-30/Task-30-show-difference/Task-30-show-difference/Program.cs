﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_30_show_difference
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Number 1.");
            int number1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter Number 2.");
            int number2 = int.Parse(Console.ReadLine());
            Console.Clear();

            if (number1 >= number2)
            {
                var total1 = number1 + number2;     /*Got confused at the question, Code shows the answer when added as a string and as a number */
                Console.WriteLine(total1);          /*Shows when they are added together */
                Console.WriteLine(number1 + number2);
            }

            if (number1 <= number2)
            {
                var total2 = number2 + number1;
                Console.WriteLine(total2);
                Console.WriteLine(number2 + number1);
            }

            
        }
    }
}
