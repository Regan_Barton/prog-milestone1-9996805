﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_32_day_of_week
{
    class Program
    {
        static void Main(string[] args)
        {
            var week = new List<Tuple<string, string, int>>();

            week.Add(Tuple.Create("Monday", "Weekday", 1));
            week.Add(Tuple.Create("Tuesday", "Weekday", 2));
            week.Add(Tuple.Create("Wednesday", "Weekday", 3));
            week.Add(Tuple.Create("Thursday", "Weekday", 4));
            week.Add(Tuple.Create("Friday", "Weekday", 5));
            week.Add(Tuple.Create("Saturday", "Weekend", 6));
            week.Add(Tuple.Create("Sunday", "Weekend", 7));

            foreach (var w in week)
            {
                Console.WriteLine($"{w.Item1} is Day {w.Item3}");
            }


        }
    }
}
        
    

