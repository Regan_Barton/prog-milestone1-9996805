﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_40_wednesdays_in_month
{
    class Program
    {
        static void Main(string[] args)
        {
            var days = 31;
            var weekdays = 7;
            var weeks = days / weekdays;
            var i = (int)Math.Ceiling((weeks) + 0.1);
            Console.WriteLine($"31 Days in a month / 7 days in a week ={i} Weeks");
            Console.WriteLine();
            Console.WriteLine("Wednesday is the third day of the week with 5 weeks = 5 Wednesdays in a month");
            /* Question is the same as task 39 but with wednesday instead of monday */
        }
    }
}
