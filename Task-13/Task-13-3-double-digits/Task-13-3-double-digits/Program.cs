﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_13_3_double_digits
{
    class Program
    {
        static void Main(string[] args)
        {
            var GST = 0.15;
            Console.WriteLine("Enter First Number.");
            double one = double.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Enter Second Number.");
            double two = double.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Enter Third Number.");
            double three = double.Parse(Console.ReadLine());
            Console.Clear();


            Console.WriteLine($"Total For First Number Is ${(one * GST) + one}");
            Console.WriteLine($"Total For Second Number Is ${(two * GST) + two}");
            Console.WriteLine($"Total For Third Number Is ${(three * GST) + three}");
            Console.WriteLine($"Total of all Numbers are: ${((one + two + three)* GST) + (one + two + three)}");


        }
    }
}