﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_39_Mondays_in_month
{
    class Program
    {
        static void Main(string[] args)
        {
            var days = 31;
            var weekdays = 7;
            var weeks = days / weekdays;
            var i = (int)Math.Ceiling((weeks) + 0.1);
            Console.WriteLine($"31 Days in a month / 7 days in a week ={i} Weeks");
            Console.WriteLine();
            Console.WriteLine("Monday first day of the week with 5 weeks = 5 mondays in a month");
            /* Question specified telling how many mondays are in a month i assumed it was math so i worte a code for this */
        }
    }
}
