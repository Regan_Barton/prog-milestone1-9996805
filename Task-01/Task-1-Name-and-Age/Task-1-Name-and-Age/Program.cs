﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1_Name_and_Age
{
    class Program
    {
        static void Main(string[] args)
        {

            /*Start of First way to print string*/
            Console.WriteLine("What is your name?");
            var name = Console.ReadLine();
            Console.Clear();

            
            Console.WriteLine($"What is your age?");
            var age = int.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine($"Your name is {name} and your age is {age}");
            Console.ReadLine();
            Console.Clear();
            /*End of First way to print string*/

            /*Start of Second way to print string*/
            Console.WriteLine("Hello What is your name?");
            var Uname = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Hello " + Uname + " What is your age?");
            var Uage = int.Parse(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("Your name is " + Uname + " and you are " + Uage + " Years old");
            Console.ReadLine();
            Console.Clear();
            /*End of Second way to print string*/

            /*Start of Third way to print string*/
            Console.WriteLine("What is your name?");
            var Tname = Console.ReadLine();
            Console.Clear();


            Console.WriteLine($"What is your age?");
            var Tage = int.Parse(Console.ReadLine());
            Console.Clear();

            Console.WriteLine("Your name is {0} and your age is {1}",Tname, Tage);
            /*End of Third way to print string*/
        }
    }
}
