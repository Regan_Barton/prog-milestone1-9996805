﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leap_year_for_next_20_years
{
    class Program
    {
        static void Main(string[] args)
        {
            var year = 2012;
            int i = 0;

            for (i = 0; i < 20;) 
            {
                i = i + 4;
                Console.WriteLine($"The next leap year is {year + i}: ");
            }
            /*I made the year variable as 2012 because that was the last leap year so the first leap year of the code is the current year, I have showed every 4 years after 2016 as a leap year for the next 20 years */
        }
    }
}
