﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leap_year_for_next_20_years
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input a Leap year."); /* User Inputs a leap year */
            var year = int.Parse(Console.ReadLine());
            int i = 0;
            var totallyear = 0;
            Console.Clear();

            for (i = 0; i < 20;)
            {
                i = i + 4;                  /* Calculates the amount of leap years after after the user input year */
                totallyear++;
            }
            Console.WriteLine($"There are going to be {totallyear} Leap years.");
        }
    }
}
