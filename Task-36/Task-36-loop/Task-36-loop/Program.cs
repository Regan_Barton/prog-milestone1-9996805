﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_36_loop
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter number for counter."); /* Description didnt say what the counter was so i made it a user input, When the counter is equal to the index it will give feedback/say when*/
            var counter = int.Parse(Console.ReadLine());
            var i = 0;
            Console.WriteLine();
            for (i = 0; i <= counter;)
            {
                if (i == counter)
                {
                    Console.WriteLine("Index is equal to the Counter");
                }
                Console.WriteLine($"{i}");
                i++;
            }
        }
    }
}
