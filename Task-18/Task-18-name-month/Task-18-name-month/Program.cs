﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_18_name_month
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = new List<Tuple<string, string, int>>();

            name.Add(Tuple.Create("Regan", "August", 20));
            name.Add(Tuple.Create("Matt", "June", 25));
            name.Add(Tuple.Create("Gurkiert", "July", 30));

            foreach (var n in name)
            {
                Console.WriteLine($"{n.Item1} , {n.Item2} , {n.Item3}");
            }

        }
    }
}
